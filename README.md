TimeEdit Unofficial
================================
Redesigned and mobile optimized timeedit webapp

## Updates

#### 2014-05-15 Separated code base from chalmerskurser.se

## Contribute

Feel free making pull requests, posting issues or ask to get write permission to the repo.

Contact me at [simonbengtsson.se](http://simonbengtsson.se) skdk@mail.com