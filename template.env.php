<?php

return array(
    "db" => array(
        "username" => "",
        "password" => "",
        "database" => "",
        "host" => ""
    ),
    "mail" => array(
        "defaultFrom" => "",
        "username" => "",
        "password" => "",
        "host" => "",
        "port" => 0,
        "ssl" => false
    ),
    'defaultChalmersUser' => '',
    'defaultChalmersPass' => ''
);