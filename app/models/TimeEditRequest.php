<?php

/**
 * Class TimeEdit
 */
class TimeEditRequest
{
    const MAX_BOOKING_TIME = 4;
    const URL_ENCODED = 'application/x-www-form-urlencoded; charset=utf-8';
    const ALL_ROOMS_URL = 'https://se.timeedit.net/web/chalmers/db1/b1/objects.json?step=1&types=186';
    const BASE_URL = 'https://se.timeedit.net/web/chalmers/db1/b1';

    protected function req($url)
    {

        $user = getenv('defaultChalmersUser');
        $pass = getenv('defaultChalmersPass');

        $h = ['Content-Type' => "application/x-www-form-urlencoded"];

        $resp = Requests::post($url, $h,
            ["authServer" => "student", "username" => $user, "password" => $pass]);

        if (!$resp->success) {
            throw new Exception("Couldn't login");
        }

        return $resp;
    }

    /**
     * Used when updating default room info
     * @return array
     */
    public function fetchAllRooms()
    {
        // All rooms
        $resp = $this->req(self::ALL_ROOMS_URL, false);
        $res = json_decode($resp->body, true);

        $rooms = [];
        foreach ($res['ids'] as $id) {
            // Get information about each room
            $resp = $this->req(self::BASE_URL . "/objects/$id/o.json?fr=t");
            $roomObj = json_decode($resp->body, true);
            $rooms[] = new Room($id, $roomObj['ID'], $roomObj['Lokaltyp'],
                $roomObj['Byggnad'], $roomObj['Utrustning'], $roomObj['Lokalstorlek']);
        }

        return $rooms;
    }

    public function fetchAvailableRooms($date, $hour, $duration)
    {
        $url = self::BASE_URL . '/objects.json?step=1&types=186&dates=';
        $url .= $date . '-' . $date . '&starttime=' . $hour . '%3A00&endtime=' . ($hour + $duration) . '%3A00';

        $resp = $this->req($url, false);
        $roomIds = json_decode($resp->body, true)['ids'];

        $availableRooms = array_where(Room::all(), function ($roomId, $room) use ($roomIds) {
            foreach ($roomIds as $id) {
                // Change $room->id to int
                if ($room->id == $id) {
                    return true;
                }
            }

            return false;
        });

        return $availableRooms;
    }

    public function cancelBooking($id)
    {
        $url = 'https://se.timeedit.net/web/chalmers/db1/b1/r.html?id=' . $id;

        $token = $this->login(Session::get('cid'), Session::get('password'));

        $headers = ['Cookie' => $token];

        $resp = Requests::delete($url, $headers);

        return $resp->body;
    }

    public function createBooking($date, $startHour, $endHour, $roomId)
    {
        if(!$this->isLoggedIn()) {
            return false;
        }
        $header = $this->login(Session::get('cid'), Session::get('password'));

        $data = 'dates=' . $date . '&datesEnd=' . $date;
        $data .= '&startTime=' . $startHour . '%3A00&endTime=' . $endHour . '%3A00';
        $data .= '&o=' . $roomId . '.186&o=203460.192';

        $data .= '&url=https%3A%2F%2Fse.timeedit.net%2Fweb%2Fchalmers%2Fdb1%2Fb1%2Fr.html';
        $data .= '%3Fh%3Dt%26sid%3D1002%26id%3D-1%26step%3D2%26id%3D-1%26';
        $data .= 'dates%3D' . $date . '%26datesEnd%3D' . $date . '%26';
        $data .= 'startTime%3D' . $startHour . '%253A00%26endTime%3D' . $endHour . '%253A00%26';
        $data .= 'o%3D' . $roomId . '.186%252C5215%26o%3D203460.192%252C%25C3%2596vrigt';

        // Use hooks to be able to have data with multiple keys with the same name
        $hooks = new Requests_Hooks();

        $hooks->register('fsockopen.before_send', function () {
            throw new Exception('Curl not installed, support fsockopen?');
        });

        $hooks->register('curl.before_send', function ($ch) use ($data) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        });

        $url = 'https://se.timeedit.net/web/chalmers/db1/b1/r.html';

        $resp = Requests::post($url, ['Cookie' => $header, 'Content-Type' => "application/x-www-form-urlencoded"], [],
            ['hooks' => $hooks]);

        return $resp->body;
    }

    /**
     * Login and returns a cookie header to be used when doing other requests
     * @param $user
     * @param $pass
     * @return bool
     */
    private function login($user, $pass)
    {
        $url = 'https://se.timeedit.net/web/chalmers/db1/b1';
        $headers = ['Content-Type' => "application/x-www-form-urlencoded"];
        $data = ["authServer" => "student", "username" => $user, "password" => $pass];
        $options = ['follow_redirects' => false];

        $resp = Requests::post($url, $headers, $data, $options);

        if ($resp->status_code === 303) {
            return $this->parseCookie($resp->headers->offsetGet('set-cookie'));
        }

        return false;
    }

    public function isLoggedIn()
    {
        return Session::get('cid') && Session::get('password');
    }

    public function saveLogin($user, $pass)
    {
        $token = $this->login($user, $pass);
        if ($token) {
            Session::put('cid', $user);
            Session::put('password', $pass);

            return $token;
        } else {
            return false;
        }
    }

    private function parseCookie($cookie)
    {
        return explode(';', $cookie)[0];
    }

    public function getBookings()
    {
        if(!$this->isLoggedIn()) {
            return false;
        }
        $header = $this->login(Session::get('cid'), Session::get('password'));
        $resp = Requests::get('https://se.timeedit.net/web/chalmers/db1/b1/my.json', ['Cookie' => $header]);

        $raw = json_decode($resp->body, true)['reservations'];

        $bookings = [];
        foreach ($raw as $b) {
            $time = $b['startdate'] . ' ' . $b['starttime'] . '-' . $b['endtime'];
            $bookings[] = new Booking($b['id'], $b['columns'][0], $time);
        }

        return $bookings;
    }
}
