<?php

/**
 * @author Simon Bengtsson
 */
class Booking
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $time;

    /**
     * @var string
     */
    public $roomInfo;

    public function __construct($id, $roomInfo, $time) {
        $this->id = $id;
        $this->roomInfo = $roomInfo;
        $this->time = $time;
    }

    public function infoString()
    {
        return $this->roomInfo;
    }

    public function roomName() {
        return $this->roomInfo;
    }
}