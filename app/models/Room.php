<?php

use \Symfony\Component\DomCrawler\Crawler;

/**
 * @author Simon Bengtsson
 */
class Room
{
    public $id;
    public $name;
    public $type;
    public $building;
    public $equipment;
    public $size;
    public $freeTime;

    function __construct($id, $name, $type, $building, $equipment, $size)
    {
        $this->id = $id;
        $this->name = $name;
        $this->type = $type;
        $this->building = $building;
        $this->equipment = $equipment;
        $this->size = $size;
    }

    /*
	|--------------------------------------------------------------------------
	| Static room array section
	|--------------------------------------------------------------------------
	*/

    const FILE_PATH = 'storage/rooms.dat';

    private static $rooms = array();

    public static function find($id) {
        return $id ? array_get(self::$rooms, $id) : null;
    }

    public static function findFromBookingId($bookingId) {
        $arr = Session::get('bookingRoomMap', []);
        return self::find(array_get($arr, $bookingId));
    }

    public static function findFromName($name)
    {
        $room = array_first(self::$rooms, function($key, $room) use ($name) {
            return $room->name === $name;
        });
        return $room;
    }

    public static function all() {
        return self::$rooms;
    }

    /**
     * Saves the given rooms to file
     * @param $rooms array The rooms to save
     */
    public static function saveRooms($rooms)
    {
        $str = serialize($rooms);
        File::put(app_path(self::FILE_PATH), $str);
        self::loadRooms();
    }

    /**
     * Loads rooms from file
     */
    public static function loadRooms()
    {
        if(is_file(app_path(self::FILE_PATH))) {
            $str = file_get_contents(app_path(self::FILE_PATH));
            self::$rooms = unserialize($str);
        } else {
            self::$rooms = (new TimeEditRequest)->fetchAllRooms();
            self::saveRooms(self::$rooms);
        }
    }
}

/**
 * Load rooms when the application starts up.
 */
Room::loadRooms();