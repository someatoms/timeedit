<?php

class RoomController extends Controller
{
    const EDIT_BUILDING = 'EDIT-Huset';
    const M_BUILDING = 'M-Huset';
    const IDE_BUILDING = 'Idéläran';
    const LIND_BUILDING = 'Others';
    const OTHER_BUILDING = 'Others';

    /**
     * @var TimeEditRequest
     */
    protected $timeEdit;

    public function __construct()
    {
        $this->timeEdit = new TimeEditRequest();
    }

    /**
     * Shows the basic page. Everything else is loaded via javascript.
     * @return \Illuminate\View\View
     */
    public function show()
    {
        $rooms = $this->sortIntoBuildings(Room::all());

        return View::make('rooms.rooms', array(
            'buildings' => $rooms,
            'title' => 'Grupprum',
            'isLoggedIn' => $this->timeEdit->isLoggedIn()
        ));
    }

    /**
     * Returns html for the rooms page
     * @return \Illuminate\View\View
     */
    public function loadAvailableRooms()
    {

        $duration = TimeEdit::MAX_BOOKING_TIME;
        $finalRooms = array();

        while ($duration > 0) {
            $rooms = $this->timeEdit->fetchAvailableRooms(Input::get('date'), Input::get('hour'), $duration);

            foreach ($rooms as $room) {
                $alreadyExist = false;
                foreach ($finalRooms as $finalRoom) {
                    if ($finalRoom->id === $room->id) {
                        $alreadyExist = true;
                        break;
                    }
                }
                if (!$alreadyExist) {
                    $room->freeTime = $duration;
                    $finalRooms[] = $room;
                }
            }

            $duration--;
        }
        $buildings = $this->sortIntoBuildings($finalRooms);

        return View::make('rooms.buildings', array(
            'buildings' => $buildings
        ));
    }

    /**
     * Returns html for the bookings page
     * @throws Exception if user is not logged in
     * @return \Illuminate\View\View
     */
    public function loadBookings()
    {
        if ($this->timeEdit->isLoggedIn()) {

            $bookings = $this->timeEdit->getBookings();

            return View::make('rooms.bookings', array(
                'bookings' => $bookings
            ));

        }

        return array('success' => false);
    }

    /**
     * Login the current user
     * @return \Illuminate\View\View
     */
    public function login()
    {
        $cid = Input::get('cid');
        $pass = Input::get('password');

        $success = $this->timeEdit->saveLogin($cid, $pass);

        if ($success) {
            $bookings = $this->timeEdit->getBookings();

            return Response::json(array(
                'success' => true,
                'html' => '' . View::make('rooms.bookings', array('bookings' => $bookings))
            ));
        }

        return Response::json(array(
            'success' => false,
            'message' => "Login failed"
        ));

    }

    /**
     * Logout the current user
     * @return \Illuminate\View\View
     */
    public function logout()
    {
        Session::remove('cid');
        Session::remove('password');

        return Response::json(array('success' => true));
    }


    /**
     * Cancels a booking
     * @param $id
     * @return mixed
     */
    public function cancelBooking($id)
    {
        $this->timeEdit->cancelBooking($id);

        return Response::json('');
    }

    /**
     * Creates a booking
     * @return array
     */
    public function createBooking()
    {
        $this->timeEdit->createBooking(Input::get('date'), Input::get('startHour'),
            Input::get('endHour'), Input::get('roomId'));

        return array('success' => true);
    }

    /**
     * Only used when room date should be updated
     */
    public function updateRoomInfo()
    {
        $fetchedRooms = $this->timeEdit->fetchAllRooms();
        Room::saveRooms($fetchedRooms);

        return 'Room info updated!';
    }

    /**
     * Sorts the rooms into a buildings array
     * @param $rooms
     * @return array Buildings
     */
    private function sortIntoBuildings($rooms)
    {
        $buildings = array(
            self::EDIT_BUILDING => array(),
            self::M_BUILDING => array(),
            self::IDE_BUILDING => array(),
            self::LIND_BUILDING => array(),
        );

        foreach ($rooms as $room) {
            switch ($room->building) {
                case self::EDIT_BUILDING:
                    if (strpos(strtolower($room->name), 'ide') === false) {
                        array_push($buildings[self::EDIT_BUILDING], $room);
                    } else {
                        array_push($buildings[self::IDE_BUILDING], $room);
                    }
                    break;
                case self::M_BUILDING:
                    array_push($buildings[self::M_BUILDING], $room);
                    break;
                // Place lindholmen and others in the same column
                //case self::LIND_BUILDING:
                default:
                    array_push($buildings[self::LIND_BUILDING], $room);
                    break;
            }
        }

        return $buildings;
    }
}

