<?php
/**
 * @var $loggedIn boolean
 * @var $bookings array
 */
?>

<?php $i = 1; ?>

<script>
    var bookingsCount = <?php echo sizeof($bookings) ?>;
</script>

@foreach($bookings as $booking)
<div id="booking{{$i}}" data-id="{{$booking->id}}" class="booking confirmed">
    <h3>Ingen bokning</h3>

    <h5 class="number">
        {{$booking->roomName()}}
        <span class="time-picker">{{$booking->time}}</span>
    </h5>

    <p class="info">
        {{ $booking->infoString() }}
    </p>

    <div class="btn-cont">
        <span id="btn-dec" class="btn">-</span>
        <span id="duration">du</span>
        <span id="btn-inc" class="btn">+</span>

        <div class="btn btn-unconfirmed-cancel">Cancel</div>
        <div class="btn btn-confirm">Confirm</div>
    </div>
    <div class="btn btn-confirmed-cancel">Cancel</div>
</div>

<?php $i++ ?>

@endforeach

@for ($j = 0; $j < 4 - sizeof($bookings); $j++)

<div id="booking{{$i}}" class="booking empty">
    <h3>Ingen bokning</h3>
    <h5 class="number">
        <span class="time-picker"></span>
    </h5>

    <p class="info"></p>

    <div class="btn-cont">
        <span class="btn btn-dec">-</span>
        <span class="duration"></span>
        <span class="btn btn-inc">+</span>

        <div class="btn btn-unconfirmed-cancel">Cancel</div>
        <div class="btn btn-confirm">Confirm</div>
    </div>
    <div class="btn btn-confirmed-cancel">Cancel</div>
</div>

<?php $i++ ?>

@endfor

<div class="logged-in-box">Inloggad som <em>{{Session::get('cid')}}</em> <a href="javascript:Rooms.logout()">Logga ut</a></div>