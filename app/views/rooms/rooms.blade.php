@extends('rooms.layout')
@section('content')

<?php
/**
 * @var boolean $isLoggedIn
 */
?>

<div class="beta">BETA - Uppdaterad {{ Config::get('app.lastUpdated') }}</div>

<script>
    var loggedIn = <?php echo $isLoggedIn ? 1 : 0; ?>;
</script>

{{-- ROOM SECTION --}}

<article id="room-section">

    <div class="actionbar-cont">
        <header class="actionbar container-fluid">
            <h1>
                <span class="glyphicon glyphicon-calendar nav-btn"></span>
                TimeEdit
            </h1>
            <nav>
                <a class="glyphicon glyphicon-refresh nav-btn update-btn"></a>
                @if($isLoggedIn && Session::get('cid') === 'simbeng')
                <a class="glyphicon glyphicon-home nav-btn" href="javascript:Rooms.openDoor()"></a>
                @endif
                <a id="user-btn" href="javascript:Rooms.showBookings()" class="glyphicon glyphicon-user nav-btn"></a>
            </nav>
        </header>
    </div>

    <div class="timebar-cont">
        <div class="timebar container-fluid">
            <div class="btn-group-horizontal hours-cont" data-toggle="buttons">
                <p>Updating...</p>
            </div>
            <input class="btn btn-date-picker" id="inputdate" data-value="-">
        </div>
    </div>

    <div class="datepicker"></div>

    <div id="buildings" class="container-fluid">
        @include('rooms.buildings')
    </div>

</article>

{{-- USER SECTION --}}

<article id="user-section">

    <div class="actionbar-cont">
        <header class="actionbar container">
            <a href="javascript:history.back()">
                <span class="glyphicon glyphicon-chevron-left nav-btn"></span>
                Bokningar
            </a>
            <nav>
                <a class="glyphicon glyphicon-refresh nav-btn update-btn"></a>
            </nav>
        </header>
    </div>

    <div id="bookings" class="container">
        <p>Updating...</p>
    </div>

</article>

{{-- LOGIN --}}

<div id="login-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="alert alert-warning">Testing alert</div>
            <h4>Inloggning</h4>

            <form id="login-form" class="clearfix">
                <div class="form-group">
                    <input type="text" class="form-control" id="cid" placeholder="Chalmers ID (CID)"/>
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="password" placeholder="Lösenord"/>
                </div>
                <button type="submit" class="btn pull-right">Logga in</button>
            </form>

        </div>
    </div>
</div>

{{-- TOAST --}}
<div id="toast">Toast</div>


{{-- SMURF --}}
<img id="smurf" src="/assets/smurf.png">

@stop
