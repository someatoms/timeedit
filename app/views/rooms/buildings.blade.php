<div class="row">
@foreach($buildings as $buildingName => $rooms)

<div class="building col-md-3">

    <h4>{{$buildingName}} ({{ sizeof($rooms) }})</h4>
    @foreach($rooms as $room)

    <div class="room" data-id="{{$room->id}}">
        <h5 class="number">{{ $room->name }}
            <?php

            if($room->freeTime) {
                $freeTime = $room->freeTime === 4 ? $room->freeTime . 'h+' : $room->freeTime . 'h';
            } else {
                $freeTime = '';
            }

            ?>
            <span class="free-time">{{ $freeTime }}</span>
        </h5>

        <p class="info">
            {{ $room->type ? ($room->type . ', ') : ''}}
            {{ $room->equipment ? ($room->equipment . ', ') : ''}}
            Platser: {{ $room->size }}
        </p>
        <span class="glyphicon glyphicon-chevron-right"></span>
    </div>

    @endforeach

</div>

@endforeach
</div>