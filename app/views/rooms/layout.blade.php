<!DOCTYPE html>

<html>
<head>
@include('includes.head')
</head>
<body id="rooms">

@yield('content')
@include('includes.scripts')
<script src="/js/rooms.js"></script>

</body>
</html>