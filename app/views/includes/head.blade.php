<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>{{$title}}</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width">
<meta name="mobile-web-app-capable" content="yes">
<link rel="icon" type="image/png" href="/assets/appicon.png">
<link rel="icon" sizes="196x196" href="/assets/appicon.png">

<link href='https://fonts.googleapis.com/css?family=Roboto:300,100,400,700,300italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="/less/styles.css">