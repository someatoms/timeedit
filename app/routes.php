<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

use Goutte\Client;

Route::any('/api/bookings', 'RoomController@loadBookings');
Route::any('/api/rooms/logout', 'RoomController@logout');
Route::any('/api/bookings/{id}', 'RoomController@cancelBooking');
Route::get('/api/createBooking', 'RoomController@createBooking');
Route::any('/api/', 'RoomController@loadAvailableRooms');
Route::any('/api/updateRoomInfo', 'RoomController@updateRoomInfo');
Route::any('/api/login', 'RoomController@login');

Route::any('/open', function() {
    $td = new TimeEdit();
    if($td->isLoggedIn() && Session::get('cid') === 'simbeng') {

        $url = 'https://portal.csbnet.se';
        $client = new Client();
        $crawler = $client->request('GET', $url);
        $form = $crawler->selectButton('Login')->form();
        $client->submit($form, array('username' => $_ENV['defaultHouseUser'], 'password' => $_ENV['defaultHousePass']));
        $link = $client->getCrawler()->filter('#open_door a')->link();
        $client->click($link);

        return ['sucess' => true];
    }
    throw new Exception("Couldn't perform door magic :(");
});
Route::any('/', 'RoomController@show');

